# 15. Use vscode Server

Date: 2023-04-10

## Status

Accepted

Supersedes [13. dev-enviroment](0013-dev-enviroment.md)

## Context

gitpod 무료 버전이라서 사용 시간이 제한이 있다.

## Decision

사내 vs code server를 직접 설치 해서 사용한다.

## Consequences

사내 전파한다.
